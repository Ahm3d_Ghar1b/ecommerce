import {useContext} from 'react'
import {ThemeContext} from '../contexts/Theme'
import Footer1 from '../Footers/Footer1'
const Nav1 = ()=>{
    const {isLightTheme,darkTheme,lightTheme,toggleTheme}=useContext(ThemeContext)
    const theme = isLightTheme? lightTheme : darkTheme
    return(
        <div className="px-5 pt-10 md:px-10 fixed z-40 hidden">
            <div className="flex justify-between">
                <h1 className="logo-font text-3xl md:text-4xl font-bold">Gaming Nation</h1>
                <div>
                    <button className="default-button" style={{background:theme.secondColor}}>Back to Home</button>
                    <button className="default-button ml-5" style={{background:theme.secondColor}}>Sign Up</button>
                </div>
                
            </div>
        </div>
    )
    
}


export default Nav1