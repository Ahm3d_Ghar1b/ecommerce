import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper.scss';
import "swiper/components/pagination/pagination.min.css"
import SwiperCore, {
    Pagination
  } from 'swiper/core';
SwiperCore.use([Pagination]);


  
const SliderContainer2 = ({children})=>{
    return(
        <Swiper
        spaceBetween={50}
        slidesPerView={3}
        pagination={{
            "clickable": true
          }}
        breakpoints={{
            "0": {
            "slidesPerView": 1,
            "spaceBetween": 0
            },
            "768": {
            "slidesPerView": 2,
            "spaceBetween": 0
            },
            "1280": {
            "slidesPerView": 3,
            "spaceBetween": 0
            }}}
        >
            {children}
        </Swiper>
    )
}

export default SliderContainer2