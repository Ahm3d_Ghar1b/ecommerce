import { useContext } from "react"
import { ThemeContext } from "../contexts/Theme"

const Header1 = ({styling,title})=>{
    const {isLightTheme,darkTheme,lightTheme}=useContext(ThemeContext)
    const theme = isLightTheme? lightTheme : darkTheme
    return(
        <div className={`${styling}`}>
            <h2 style={{borderColor:theme.secondColor}} className={`headers border-b-4 uppercase`}>{title}</h2>
        </div>
    )
}

export default Header1