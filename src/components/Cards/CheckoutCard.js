import { useContext } from "react"
import { Link } from "react-router-dom"
import { ThemeContext } from "../contexts/Theme"
const CheckoutCard = ({cartItem})=>{
    const {isLightTheme,darkTheme,lightTheme}=useContext(ThemeContext)
    const theme = isLightTheme? lightTheme : darkTheme
    return(
        <div style={{background:theme.cartColor}} className="flex lg:flex-row flex-col justify-between pb-8 pl-10 pr-5 pt-5 shadow-lg rounded-xl mt-5">
            <div>
                <h2 className="font-semibold lg:mb-2 text-gray-400">Product Details</h2>
                <div className="flex lg:flex-row flex-col">
                    <img className="w-36 object-cover rounded-xl" src={cartItem.image1} alt={cartItem.name}/>
                    <div className="lg:ml-2 flex flex-col justify-between font-semibold lg:mt-0 mt-2">
                        <Link to={`/${cartItem.name}/${cartItem.gamesId}`} className="tracking-wider sm:w-auto text-lg">{cartItem.name}</Link>
                        <p style={{color:theme.secondColor}}>{cartItem.category}</p>
                    </div>
                </div>
            </div>
            <div className="flex lg:flex-row flex-col lg:text-center">
                <div className="font-semibold lg:mt-0 mt-3">
                    <h2 className="font-semibold lg:mb-2 text-gray-400">Item's Total Price</h2>
                    <p style={{color:theme.secondColor}}>{cartItem.priceUpdated*cartItem.quantity}</p>
                </div>
                <div className="font-semibold lg:ml-5 lg:mt-0 mt-3">
                    <h2 className="font-semibold lg:mb-2 text-gray-400">Item's Total Profit</h2>
                    <p style={{color:theme.secondColor}}>{cartItem.profitUpdated}</p>
                </div>
            </div>
        </div>
    )
}

export default CheckoutCard