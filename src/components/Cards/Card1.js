import { useState,useEffect } from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShoppingCart } from "@fortawesome/free-solid-svg-icons"
import {useContext} from 'react'
import {ThemeContext} from '../../components/contexts/Theme'
import axios from "axios"
import Loading1 from "../Loadings/Loading1"
import { Link } from "react-router-dom"
import { useDispatch } from "react-redux"
import * as actionCreators from '../../store/actions'
const Card1 = ({game,styling})=>{
    const {isLightTheme,darkTheme,lightTheme}=useContext(ThemeContext)
    const theme = isLightTheme? lightTheme : darkTheme
    const [cartState,setCartState]=useState(true)
    const [itemState,setItemState]=useState(game.itemInCart)
    const [loading,setLoading]=useState(false)
    const [rerender,setRerender]=useState(true)
    const dispatch = useDispatch()
    useEffect(()=>{
        dispatch(actionCreators.fetchGames())
    },[rerender])
    const handleCartState = () =>{
        if(cartState && !itemState){
            setCartState(false)
            setLoading(true)
            axios.patch(`https://ecommerce-6f468-default-rtdb.firebaseio.com/games/${game.id}.json`,{itemInCart:true}).then(response=>{
                setItemState(true)
                setLoading(false)
                setRerender(!rerender)
            })
            axios.post('https://ecommerce-6f468-default-rtdb.firebaseio.com/cart.json',{...game,quantity:1,profitUpdated:game.profit,priceUpdated:game.price,gamesId:game.id,oldQuantity:game.quantity,date:new Date()}).then(response=>{
                setLoading(false)
                
            })
        }else{
            return null
        }
    }
    return(
        <div>
            <div className={`relative mb-5 ${styling} lg:mx-4 mx-3`}>
                <Link to={`/${game.name}/${game.id}`}>
                    <img src={`/${game.image1}`} className="object-cover h-full w-full rounded-t-lg" alt={game.name}/>
                    <div style={{background:theme.cartColor}} className="px-5 pb-5 pt-3 shadow-xl">
                        <div style={{color:theme.secondColor}} className="flex justify-between">
                            <span>{game.category}</span>
                            <span>Profit:{game.profit}$</span>
                        </div>
                        <div>
                            <h2 className="font-bold text-xl pb-1">{game.name}</h2>
                            <p className="font-light">{game.description.substring(0,80)+'...'}<Link to={`/${game.name}/${game.id}`} style={{color:theme.secondColor}} className="hover:border-b-4">Read More</Link></p>
                        </div>
                    </div>
                </Link>
                <div style={{background:!itemState?theme.secondColor:'rgba(0,0,0,0.75)'}} className={`flex justify-between px-5 py-4 rounded-b-lg ${cartState && !itemState?'cursor-pointer':'cursor-not-allowed'}`} onClick={handleCartState}>
                    <div>
                        <FontAwesomeIcon icon={faShoppingCart} className="text-white text-lg"/>
                        <span className="text-white font-semibold text-lg ml-2">{!itemState?'Add to Cart':'Added To Cart'}</span>
                    </div>
                    {loading?<svg class="animate-spin h-5 w-5 mr-3 ..." viewBox="0 0 24 24"></svg>:<span className="text-white font-semibold text-lg">{game.price}$</span>}
                </div>
            </div>
            {loading?<Loading1/>:null}
        </div>
    )
}

export default Card1