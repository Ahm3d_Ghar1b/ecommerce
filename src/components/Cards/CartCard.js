import { useContext, useEffect, useState } from "react"
import { useDispatch,useSelector } from "react-redux"
import * as actionCreators from '../../store/actions'
import { ThemeContext } from "../contexts/Theme"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from "@fortawesome/free-solid-svg-icons"
import { faMinus } from "@fortawesome/free-solid-svg-icons"
import { faTrash } from "@fortawesome/free-solid-svg-icons"
import { faCheck } from "@fortawesome/free-solid-svg-icons"
import { Link } from "react-router-dom"
const CartCard = ({cartItem,deleteItem,confirmItem})=>{
    const {isLightTheme,darkTheme,lightTheme}=useContext(ThemeContext)
    const theme = isLightTheme? lightTheme : darkTheme
    const cart = useSelector (state=>state.cart)
    const [rerender,setRerender]=useState(true)
    const [error,setError]=useState('')
    const [itemPriceUpdate,setItemPriceUpdate]=useState(cartItem.priceUpdated)
    const [itemQuantity,setItemQuantity]=useState(cartItem.quantity)
    const dispatch = useDispatch()
    useEffect(()=>{
        dispatch(actionCreators.fetchCart())
    },[rerender])
    const addCounter = ()=>{
        setItemQuantity(itemQuantity+1)
    }
    const subtractCounter = ()=>{
        if(itemQuantity>1){
            setItemQuantity(itemQuantity-1)
        }
    }
    
    return(
        <div className="relative">
            <div style={{background:theme.cartColor}} className={`${(parseFloat(cartItem.profit)+parseFloat((itemPriceUpdate-cartItem.price)))*itemQuantity<0 || itemQuantity>cartItem.oldQuantity?'pt-4':'pt-5'} pb-16 pl-10 pr-5 shadow-lg rounded-xl mt-5`}>
                <div className={`text-red-600 text-center mb-4 ${(parseFloat(cartItem.profit)+parseFloat((itemPriceUpdate-cartItem.price)))*itemQuantity<0 || itemQuantity>cartItem.oldQuantity?'block':'hidden'}`}>
                    <p>{itemQuantity>cartItem.oldQuantity?`That Quantity is not available`:null}</p>
                    <p>{(parseFloat(cartItem.profit)+parseFloat((itemPriceUpdate-cartItem.price)))*itemQuantity<0?'Profit cannot be less than 0':null}</p>
                </div>
                <div className="flex lg:flex-row flex-col justify-between">
                    <div className="w-80">
                        <h2 className="font-semibold lg:mb-2 text-gray-400">Product Details</h2>
                        <div className="flex sm:flex-row flex-col">
                            <img className="w-36 object-cover rounded-xl" src={cartItem.image1} alt={cartItem.name}/>
                            <div className="sm:ml-2 flex flex-col justify-between font-semibold sm:mt-0 mt-2">
                                <Link to={`/${cartItem.name}/${cartItem.gamesId}`} className="tracking-wider sm:w-auto text-lg">{cartItem.name}</Link>
                                <p style={{color:theme.secondColor}}>{cartItem.category}</p>
                            </div>
                        </div>
                    </div>
                    <div className="font-semibold">
                        <h2 className="lg:mb-2 text-gray-400 lg:mt-0 mt-5">Profit</h2>
                        <p style={{color:theme.secondColor}}>{(parseFloat(cartItem.profit)+parseFloat((itemPriceUpdate-cartItem.price)))*itemQuantity}$</p>
                    </div>
                    <div className="lg:text-center">
                        <h2 className="font-semibold lg:mb-2 mb-1 text-gray-400 lg:mt-0 mt-5">Item's Price</h2>
                        <input style={{background:theme.secondColor}} className="text-white py-1 w-3/4 font-semibold text-center outline-none" type="number" value={itemPriceUpdate} onChange={(e)=>setItemPriceUpdate(e.target.value)}/>
                    </div>
                    <div className="lg:text-center">
                        <h2 className="font-semibold lg:mb-2 mb-1 text-gray-400 lg:mt-0 mt-5">Quantity</h2>
                        <div>
                            <div style={{background:theme.secondColor}} className="counters sm:px-3 px-2" onClick={addCounter}>
                            <FontAwesomeIcon icon={faPlus} />
                            </div>
                            <input style={{background:theme.secondColor}} className="text-center mx-1 py-1 sm:w-24 w-2/4 outline-none rounded-2xl text-white font-bold" type="number" placeholder="1" readOnly="readOnly" onChange={(e)=>setItemQuantity(e.target.value)} value={itemQuantity}/>
                            <div style={{background:theme.secondColor}} className="counters sm:px-3 px-2" onClick={subtractCounter}>
                                <FontAwesomeIcon icon={faMinus} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button className="bg-red-600  text-white font-semibold absolute bottom-0 right-0 py-2 px-3 rounded-tl-xl" onClick={()=>deleteItem(cartItem,itemQuantity)}>
                <FontAwesomeIcon icon={faTrash}/>
            </button>
            <button className={`${itemQuantity==cartItem.quantity && itemPriceUpdate==cartItem.priceUpdated?'bg-gray-600 cursor-text':'bg-green-600'} text-white font-semibold absolute bottom-0 left-0 py-2 px-3 rounded-tr-xl`} onClick={()=>confirmItem(cartItem,itemPriceUpdate,itemQuantity)}>
                <FontAwesomeIcon icon={faCheck}/>
            </button>
        </div>
    )
}

export default CartCard