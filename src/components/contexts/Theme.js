import { createContext,useState } from "react";
export const ThemeContext = createContext()
const Theme = (props)=>{
    const [isLightTheme,setIsLightTheme]=useState(true)
    const [darkTheme,setDarkTheme]=useState({
        mainColor:'#222222',
        secondColor:'#F0A500',
        cartColor:'#262626',
        textColor:'#fff'
    })
    const [lightTheme,setLightTheme]=useState({
        mainColor:'#FFF8E7',
        secondColor:'#FF9900',
        cartColor:'#fffae7',
        textColor:'#000'
    })
    const toggleTheme=()=>{
        setIsLightTheme(!isLightTheme)
    }
    return(
        <ThemeContext.Provider value={{isLightTheme,darkTheme,lightTheme,toggleTheme}} >
            {props.children}
        </ThemeContext.Provider>
    )
}

export default Theme