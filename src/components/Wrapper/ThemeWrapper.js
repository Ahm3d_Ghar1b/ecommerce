import {useContext} from 'react'
import {ThemeContext} from '../contexts/Theme'
const ThemeWrapper = (props)=>{
    const {isLightTheme,darkTheme,lightTheme}=useContext(ThemeContext)
    const theme = isLightTheme? lightTheme : darkTheme
    return(
        <div style={{background:theme.mainColor,color:theme.textColor}} className="transition-all duration-300 min-h-screen h-full relative">
            {props.children}
        </div>
    )
}

export default ThemeWrapper