import { useContext, useEffect, useState } from "react"
import { ThemeContext } from "../contexts/Theme"
const Loading2=({children,bg})=>{
    const {isLightTheme,darkTheme,lightTheme}=useContext(ThemeContext)
    const theme = isLightTheme? lightTheme : darkTheme
    const [loading,setLoading]=useState('show')
    const [scroll,setScroll]=useState('stop-scroll')
    useEffect(()=>{
        setTimeout(function(){
            setLoading('hide')
            setScroll('')
        },1000)
    },[])
    return(
        <div className={`${scroll}`}>
            <div style={{background:bg}} className={`overlay-centered full-page-loading ${loading}`}>
                <div className="lds-default"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
            </div>
            {children}
        </div>
    )
}

export default Loading2