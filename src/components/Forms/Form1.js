import {useContext} from 'react'
import {ThemeContext} from '../../components/contexts/Theme'
import { Link } from 'react-router-dom'
const Form = (props) =>{
    const {isLightTheme,darkTheme,lightTheme}=useContext(ThemeContext)
    const theme = isLightTheme? lightTheme : darkTheme
    return(
        <div className="h-screen">
            <div className="content-center-custom">
                <h1 className="logo-font text-4xl md:text-5xl font-bold text-left inline-block">Gaming Nation</h1>
                <p className="text-red-600">{props.signError}</p>
                <form onSubmit={props.formHandle} className="mt-10 mx-5 width-95">
                    {props.children}
                    <label className="text-gray-400">Email*</label><br/>
                    <input type="email" placeholder="Please Enter Your Email" className={`input-style ${props.emailError?'mb-1':'mb-3'}`} onChange={props.email} value={props.emailValue}/><br/>
                    <p className="text-red-600">{props.emailError}</p>
                    <label className="text-gray-400">Password*</label><br/>
                    <input type="password" placeholder="Please Enter Your Password" className="input-style" onChange={props.password} value={props.passwordValue}/><br/>
                    <p className="text-red-600">{props.passwordError}</p>
                    <div className={`flex sm:flex-row flex-col text-center sm:justify-between ${props.passwordError?'mt-2':'mt-5'}`}>
                        <button style={{background:theme.secondColor}} className="default-button px-12 rounded-lg">{props.formType}</button>
                        <Link to="/" style={{background:theme.secondColor}} className="default-button px-12 rounded-lg mt-3 sm:mt-0">Back to Home</Link>
                    </div>
                    <p className="text-gray-500 mt-2">{props.account} <Link to={props.formLink} style={{color:theme.secondColor}} className="cursor-pointer font-semibold">{props.formLinkTitle}</Link></p>
                </form>
            </div>
        </div>
    )
}

export default Form