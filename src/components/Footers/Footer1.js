import {useContext, useState} from 'react'
import {ThemeContext} from '../contexts/Theme'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSun } from '@fortawesome/free-solid-svg-icons'
import { faMoon } from '@fortawesome/free-solid-svg-icons'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'
import { faHome } from '@fortawesome/free-solid-svg-icons'
import { faSignInAlt } from '@fortawesome/free-solid-svg-icons'
import { useHistory } from 'react-router-dom'
const Footer1 = () =>{
    const [menuClass,setMenuClass]=useState(false)
    let history = useHistory()
    const {isLightTheme,darkTheme,lightTheme,toggleTheme}=useContext(ThemeContext)
    const theme = isLightTheme? lightTheme : darkTheme

    /*const handleMenuClass = ()=>{
        setMenuClass(!menuClass)
    }
    const handleRedirect=(link)=>{
        window.location.replace(link)
    }*/
    const handleCart = ()=>{
        window.location.replace('/cart')
    }
    return(
        <div>
            {/*<div style={{background:theme.secondColor}} className="fixed top-24 right-0 text-xl z-50 text-white rounded-full flex flex-row items-center">
                <div className="cursor-pointer -mr-0.5 px-3 py-2" onClick={handleMenuClass}>
                    <FontAwesomeIcon icon={faBars}/>
                </div>
                <div className={`flex flex-row ml-5 ${!menuClass?'-mr-28':'mr-2'} transition-all py-2 duration-300`} onClick={handleMenuClass}>
                    <FontAwesomeIcon className="mr-3 cursor-pointer" icon={faShoppingCart} onClick={()=>handleRedirect('/cart')}/>
                    <FontAwesomeIcon className="mr-3 cursor-pointer" icon={faHome} onClick={()=>handleRedirect('/')}/>
                    <FontAwesomeIcon className="mr-1 cursor-pointer" icon={faSignInAlt} onClick={()=>handleRedirect('/signup')}/>
                </div>
    </div>*/}
            <div className="fixed bottom-0 m-5 z-40">
                <FontAwesomeIcon style={{background:theme.secondColor}} className="p-3 text-5xl text-white rounded-xl cursor-pointer" icon={isLightTheme?faMoon:faSun} onClick={toggleTheme}/>
            </div>
            <div className="fixed bottom-0 right-0 m-5 z-40">
                <FontAwesomeIcon style={{background:theme.secondColor}} className="p-3 text-5xl text-white rounded-xl cursor-pointer" icon={faShoppingCart} onClick={handleCart}/>
            </div>
        </div>
    )   
    
}

export default Footer1