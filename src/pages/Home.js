import { useContext, useEffect, useState } from "react"
import { useSelector,useDispatch } from "react-redux"
import * as actionCreators from '../store/actions'
import Card1 from "../components/Cards/Card1"
import AwesomeSlider from 'react-awesome-slider';
import 'react-awesome-slider/dist/styles.css';
import AnimationStyles from 'react-awesome-slider/src/styled/scale-out-animation/scale-out-animation.scss';
import { ThemeContext } from "../components/contexts/Theme";
import Header1 from "../components/Headers/Header1";
import SliderContainer2 from "../components/SliderContainer/SliderContainer2";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTh } from "@fortawesome/free-solid-svg-icons";
import { faSlidersH } from "@fortawesome/free-solid-svg-icons";
import { SwiperSlide } from 'swiper/react';
import background1 from '../assets/images/leaderboards-hero-banner.jpg'
import background2 from '../assets/images/Retake-hero-poster.jpg'
const Home = ()=>{
    const {games} = useSelector((state)=>({...state}))
    const dispatch = useDispatch()
    const [categoriesDesign,setCategoriesDesign]=useState(true)
    const {isLightTheme,darkTheme,lightTheme}=useContext(ThemeContext)
    const theme = isLightTheme? lightTheme : darkTheme
    useEffect(()=>{
        dispatch(actionCreators.fetchGames())
    },[])
    const handleCategoriesDesign=()=>{
        setCategoriesDesign(!categoriesDesign)
    }
    const Slides = (category,styling)=>(
        <div>
            <Header1 styling={styling} title={category+' Category'}/>
            <SliderContainer2 > 
                {games?.filter(filteredGames=>filteredGames.category==category).map((game,gameIndex)=>{
                    return(
                        <SwiperSlide className="mb-7" key={gameIndex}>
                            <Card1 game={game}/>
                        </SwiperSlide>
                    )
                })}
            </SliderContainer2>
        </div>
    )
    return(
        <div>
            <div className="height-80vh relative">
                <AwesomeSlider className="h-screen" animation="scaleOutAnimation">
                    <div data-src={background1}/>
                    <div data-src={background2}/>
                </AwesomeSlider>
                <div className="overlay-centered overlay-color z-10 text-gray-300 tracking-widest uppercase">
                    <div className="text-center">
                        <p className="md:text-2xl text-xl">Look,Decide and Buy</p>
                        <h1 style={{color:theme.secondColor}} className="lg:text-6xl md:text-4xl text-3xl mt-10 font-semibold">Games For Everyone</h1>
                        <div>
                            <div className="mt-5 py-3 px-7 text-3xl bg-black tracking-wider relative button-container inline-block">
                                <p className="opacity-0">Play for Free</p>
                                <a href="#games" style={{background:theme.secondColor}} className="absolute top-0 left-0 h-full w-0 overflow-hidden transition-all duration-500">
                                </a>
                                <a href="#games" className="py-3 absolute top-0 left-0 h-full w-full text-white uppercase font">Buy Games</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="main-container" id="games">
                <div className='flex justify-between items-end'>
                    <div></div>
                    <div style={{background:theme.secondColor}} onClick={handleCategoriesDesign} className="text-lg mb-4 inline py-2 px-5 cursor-pointer text-white">
                        <button className="font-semibold">{categoriesDesign?'All Products':'Categories'}</button>
                        <FontAwesomeIcon icon={categoriesDesign?faTh:faSlidersH} className="ml-2"/>
                    </div>
                </div>
                <Header1 styling={!categoriesDesign?'mb-5':'mb-0'} title={!categoriesDesign?'All Categories':null}/>
                {categoriesDesign?
                <div>
                    {Slides('Adventure','mb-5')}
                    {Slides('Action','mt-5 mb-5')}
                    {Slides('Horror','mt-5 mb-5')}
                </div>:
                <div className="grid lg:grid-cols-3 md:grid-cols-2 grid-cols-1">
                    {games?.filter(filteredGames=>filteredGames.quantity).map((game,gameIndex)=>{
                        return(
                            <div key={gameIndex}>
                                <Card1 game={game} styling={!categoriesDesign?'mb-7':null}/>
                            </div>
                        )
                    })}
                </div>}
            </div>
            
        </div>
    )
}

export default Home