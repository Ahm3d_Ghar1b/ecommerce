import {useContext, useEffect, useState } from "react"
import * as actionCreators from '../store/actions'
import { useSelector,useDispatch } from "react-redux"
import CartCard from "../components/Cards/CartCard"
import { ThemeContext } from "../components/contexts/Theme"
import axios from "axios"
import Loading2 from "../components/Loadings/Loading2"
import { Link } from "react-router-dom"
const Cart = ()=>{
    const {isLightTheme,darkTheme,lightTheme}=useContext(ThemeContext)
    const theme = isLightTheme? lightTheme : darkTheme
    const cart = useSelector (state=>state.cart)
    const [loading,setLoading]=useState(false)
    const [rerender,setRerender]=useState(true)
    const dispatch = useDispatch()
    useEffect(()=>{
        dispatch(actionCreators.fetchCart())
    },[rerender])
    const deleteItem=(cartItem)=>{
        setLoading(true)
        axios.delete(`https://ecommerce-6f468-default-rtdb.firebaseio.com/cart/${cartItem.id}.json`).then(response=>{
            setRerender(!rerender)
            setLoading(false)
        })
        axios.patch(`https://ecommerce-6f468-default-rtdb.firebaseio.com/games/${cartItem.gamesId}.json`,{itemInCart:false}).then(response=>{
            console.log(response.data)
            setRerender(!rerender)
        })
       
    }
    const confirmItem=(cartItem,itemPriceUpdate,itemQuantity)=>{
        if((itemQuantity==cartItem.quantity && itemPriceUpdate==cartItem.priceUpdated) || itemPriceUpdate[0]==0){
        }else{
            if((parseFloat(cartItem.profit)+parseFloat((itemPriceUpdate-cartItem.price)))*itemQuantity<0){
            }else{
                if(itemQuantity>cartItem.oldQuantity){
                }else{
                    setLoading(true)
                    axios.patch(`https://ecommerce-6f468-default-rtdb.firebaseio.com/cart/${cartItem.id}.json`,{priceUpdated:parseFloat(itemPriceUpdate),quantity:itemQuantity,profitUpdated:(parseFloat(cartItem.profit)+parseFloat((itemPriceUpdate-cartItem.price)))*itemQuantity}).then(response=>{
                        setLoading(false)
                        setRerender(!rerender)
                    })
                }
                
            }
            
        }
    }
    const handleHome = ()=>{
        window.location.replace('/')
    }
    return(
        <div className="main-container">
            <div>
                <div className="flex justify-between sm:text-xl font-bold border-b-2 mb-3 pb-3 border-gray-100">
                    <h1>Shopping Cart</h1>
                    <p>{cart?.length} Items</p>
                </div>
                {cart?.map(cartItem=>{
                    return(
                        <div key={cartItem.id} >
                           <CartCard 
                            cartItem={cartItem} 
                            confirmItem={(cartItem,itemPriceUpdate,itemQuantity)=>confirmItem(cartItem,itemPriceUpdate,itemQuantity)} 
                            deleteItem={(cartItem,itemQuantity)=>deleteItem(cartItem,itemQuantity)}
                            />
                        </div>
                    )
                })}
            </div>
            {cart?.length==0?<div className="text-center">
                <p className="text-2xl font-bold mt-5 mb-5">No Items To Show</p>
                <button to='/' style={{background:theme.secondColor}} className="px-10 py-3 text-white font-bold" onClick={handleHome}>Back to Home</button>
            </div>:null}
            <div>{loading?<Loading2 bg='rgba(0,0,0,0.85)'/>:null}</div>
            {cart?.length!==0?<div className="flex justify-between">
                <Link to="/checkout" style={{background:theme.secondColor}} className="default-button inline-block px-5 mt-6">Compelete Your Purchase</Link>
                <Link to="/" style={{background:theme.secondColor}} className="default-button inline-block px-5 mt-6">Back to Home</Link>
            </div>:null}
        </div>
    )
}

export default Cart