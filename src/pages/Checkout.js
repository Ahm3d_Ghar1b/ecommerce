import axios from "axios"
import { useContext, useEffect, useState } from "react"
import { useSelector, useDispatch } from "react-redux"
import CheckoutCard from "../components/Cards/CheckoutCard"
import { ThemeContext } from "../components/contexts/Theme"
import Loading2 from "../components/Loadings/Loading2"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck } from "@fortawesome/free-solid-svg-icons"
import * as actionCreators from '../store/actions'
import { Link } from "react-router-dom"
const Checkout = ()=>{
    const {isLightTheme,darkTheme,lightTheme}=useContext(ThemeContext)
    const theme = isLightTheme? lightTheme : darkTheme
    const cart = useSelector (state=>state.cart)
    const [length,setLength]=useState(0)
    let [totalPrice,setTotalPrice]=useState(0)
    let [totalProfit,setTotalProfit]=useState(0)
    const [orderDone,setOrderDone]=useState(false)
    const [rerender,setRerender]=useState(true)
    const [loading,setLoading]=useState(false)
    const dispatch = useDispatch()
    useEffect(()=>{
        dispatch(actionCreators.fetchCart())
        if(length==0 && cart?.length>0){
            {cart?.forEach(item=>{
                setTotalPrice(totalPrice+=item.priceUpdated*item.quantity)
                setTotalProfit(totalProfit+=item.profitUpdated)
                setLength(1)
            })}
            setRerender(!rerender)
        }
    },[rerender])
    const handleHome = ()=>{
        window.location.replace('/')
    }
    const handleConfirmOrder=()=>{
        setLoading(true)
        axios.post('https://ecommerce-6f468-default-rtdb.firebaseio.com/completedOrders.json', cart).then(response=>{
            setLoading(false)
            setOrderDone(true)
            setTimeout(()=>{
                setOrderDone(false)
            },2000)
        })        
    }
    return(
        <div className="main-container">
            <div>
                <div className="flex justify-between sm:text-xl font-bold border-b-2 mb-3 pb-3 border-gray-100">
                    <h1>Checkout</h1>
                    <p>{cart?.length} Items </p>
                </div>
                <div  className="mt-3">
                    {cart?.map(cartItem=>{
                        return(
                            <CheckoutCard cartItem={cartItem} key={cartItem.id}/>
                        )
                    })}
                </div>
            </div>
            {cart?.length==0?<div className="text-center">
                <p className="text-2xl font-bold mt-5 mb-5">No Items To Show</p>
                <button to='/' style={{background:theme.secondColor}} className="px-10 py-3 text-white font-bold" onClick={handleHome}>Back to Home</button>
            </div>:
                <div style={{background:theme.cartColor}} className="mt-7 p-10 shadow-xl rounded-xl">
                    <h3 className="font-semibold text-xl">Total Price : {totalPrice}</h3>
                    <h3 className="font-semibold text-xl mt-5 border-b-2 pb-2">Total Profit : {totalProfit}</h3>
                    {loading?<Loading2 bg='rgba(0,0,0,0.85)'/>:null}
                    {cart?.length!==0?<div className="flex justify-between">
                        <button to="/checkout" style={{background:theme.secondColor}} className="default-button inline-block px-5 mt-3" onClick={handleConfirmOrder}>Confirm Order</button>
                        <button to="/" style={{background:theme.secondColor}} className="default-button inline-block px-5 mt-3" onClick={handleHome}>Back to Home</button>
                    </div>:null}
                </div>
            }
            <div className={`fixed flex items-center bottom-5 left-1/2 -ml-28 text-white bg-green-600 px-7 py-3 font-semibold transition-all duration-300 ${orderDone?'opacity-1':'opacity-0'}`}>
                <FontAwesomeIcon icon={faCheck}/>
                <p className="ml-2">Thank you for your order</p>
            </div>
        </div>
    )
}
export default Checkout