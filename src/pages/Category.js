import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useParams } from "react-router"
import Card1 from "../components/Cards/Card1"
import * as actionCreators from '../store/actions'
const Category = ()=>{
    const {games} = useSelector((state)=>({...state}))
    const dispatch = useDispatch()
    useEffect(()=>{
        dispatch(actionCreators.fetchGames())
    },[])
    const {item_category} = useParams()
    return(
        <div className="md:px-20 px-7 py-20">
            <div className="grid lg:grid-cols-3 md:grid-cols-2 grid-cols-1">
                {games.filter(filteredGame=>filteredGame.category==item_category).map((game,gameIndex)=>{
                    return(
                        <Card1 game={game} key={gameIndex}/>
                    )
                })}
            </div>
        </div>
    )
}

export default Category