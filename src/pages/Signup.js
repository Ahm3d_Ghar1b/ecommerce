import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'
import { useHistory } from 'react-router'
import { useState } from "react"
import Form from "../components/Forms/Form1"
import ThemeWrapper from "../components/Wrapper/ThemeWrapper"
let firebaseConfig = {
    apiKey: "AIzaSyAsdBFi40XkpcQVJ8aHhCmjzmf2EMsDsTg",
    authDomain: "ecommerce-6f468.firebaseapp.com",
    projectId: "ecommerce-6f468",
    storageBucket: "ecommerce-6f468.appspot.com",
  };
  firebase.initializeApp(firebaseConfig);
const Signup = ()=>{
    const [email,setEmail]=useState('')
    const [password,setPassword]=useState('')
    const [username,setUsername]=useState('')
    const [emailError,setEmailError]=useState('')
    const [passwordError,setPasswordError]=useState('')
    const [usernameError,setUsernameError]=useState('')
    const [signupError,setSignupError]=useState('')
    let history=useHistory()
    const handleSignUp = (e)=>{
        e.preventDefault()
        if(email.trim()===''){
            setEmailError('Your email cannot be empty')
        }else{
            setEmailError('')
        }
        if(password.trim()==='' || password.length<6){
            setPasswordError('Your password cannot be empty or below 6 letters')
        }else{
            setPasswordError('')
        }
        if(username.trim()===''){
            setUsernameError('Your username cannot be empty')
        }else{
            setUsernameError('')
        }
        if(email.trim()!=='' && password.trim()!=='' && !password.length<6 && username.trim()!==''){
            firebase.auth().createUserWithEmailAndPassword(email,password).then(cred=>{
                setEmail('')
                setPassword('')
                setUsername('')
                history.push('/login')
                firebase.auth().onAuthStateChanged(user=>{
                    console.log(user)
                })
            }).catch(err=>{
                setSignupError('Failed to Sign Up')
            })
        }
    }
    return(
        <ThemeWrapper>
            
            <Form 
            formType="Signup"
            formLink="/login"
            formLinkTitle="Login"
            account="Have an account?"
            email={(e)=>setEmail(e.target.value)}
            emailValue={email}
            password={(e)=>setPassword(e.target.value)}
            passwordValue={password}
            formHandle={handleSignUp}
            emailError={emailError}
            passwordError={passwordError}
            signError={signupError}
            >
                <label className="text-gray-400">Username*</label><br/>
                <input type="text" placeholder="Please Enter Your Username" className={`input-style ${usernameError?'mb-1':'mb-3'}`} onChange={(e)=>setUsername(e.target.value)} value={username}/><br/>
                <p className="text-red-600">{usernameError}</p>
            </Form>
        </ThemeWrapper>
    )
}

export default Signup