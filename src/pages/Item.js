import axios from "axios"
import { useContext, useEffect, useState } from "react"
import { useParams } from "react-router"
import { Link } from "react-router-dom"
import { ThemeContext } from "../components/contexts/Theme"
import Loading1 from "../components/Loadings/Loading1"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from "@fortawesome/free-solid-svg-icons"
import { faMinus } from "@fortawesome/free-solid-svg-icons"
import { useDispatch, useSelector } from "react-redux"
const Item = ()=>{
    const {games} = useSelector((state)=>({...state}))
    const dispatch = useDispatch()
    const {item_id} = useParams()
    const [itemData,setItemData]=useState('')
    const [imageDisplayed,setImageDisplayed]=useState('')
    const [loading,setLoading]=useState(false)
    const [imageLoading,setImageLoading]=useState(false)
    const [cartState,setCartState]=useState(true)
    const [itemQuantity,setItemQuantity]=useState(1)
    const [quantityError,setQuantityError]=useState('')
    const [colorChosen,setColorChosen]=useState('')
    const [rerender,setRerender]=useState(true)
    const [itemState,setItemState]=useState(itemData.itemInCart)
    const {isLightTheme,darkTheme,lightTheme}=useContext(ThemeContext)
    const theme = isLightTheme? lightTheme : darkTheme
    const images = [
        {
            id:1,
            src:itemData.image2
        },
        {
            id:2,
            src:itemData.image1
        },
        {
            id:3,
            src:itemData.image3
        }
    ]
    useEffect(()=>{
        setImageLoading(true)
        axios.get(`https://ecommerce-6f468-default-rtdb.firebaseio.com/games/${item_id}.json`).then(response=>{
            setItemData(response.data)
            setImageDisplayed(response.data.image)
            setItemState(response.data.itemInCart)
            setImageLoading(false)
        })
    },[rerender])
    //Slider Options
    
    //Set Image
    const handleImageDisplayed=(image)=>{
        setImageDisplayed(image)
    }   
    const handleItemToCart = ()=>{
        if(cartState && !itemState && itemData.quantity>=itemQuantity && itemQuantity>0 && colorChosen){
            setCartState(false)
            setLoading(true)
            setQuantityError('')
            axios.patch(`https://ecommerce-6f468-default-rtdb.firebaseio.com/games/${item_id}.json`,{itemInCart:true}).then(response=>{
                setRerender(!rerender)
            })  
            axios.post('https://ecommerce-6f468-default-rtdb.firebaseio.com/cart.json',{...itemData,quantity:itemQuantity,colorChosen:colorChosen,profitUpdated:itemData.profit,priceUpdated:itemData.price,gamesId:item_id,oldQuantity:itemData.quantity,date:new Date()}).then(response=>{
                setRerender(!rerender)
                setLoading(false)
            })
        }else if(!colorChosen && !itemState){
            setQuantityError(`Please Choose a Color`)
        }else if(itemData.quantity==0 && !itemState){
            setQuantityError(`Item Is not Available`)
        }else if (!itemState){
            setQuantityError('Item Already in Cart')
        }else{
            if(!itemState){
                setQuantityError(`Only ${itemData.quantity} items Available`)
            }
            
        }
    }
    //Quantity Counters
    const addCounter = ()=>{
        setItemQuantity(itemQuantity+1)
    }
    const subtractCounter = ()=>{
        if(itemQuantity>1){
            setItemQuantity(itemQuantity-1)
        }
    }
    //Handle Render
    /*const handleRender = (gameItem) =>{
        setColorChosen('')
        setItemQuantity(1)
        setQuantityError('')
        setRerender(!rerender)
    }*/
    return(
        <div className="main-container">
            <div className="grid lg:grid-cols-2 grid-cols-1 lg:space-x-5">
                {!imageLoading?<div className="flex flex-col justify-between">
                    <img src={imageDisplayed?imageDisplayed:itemData.image1} alt={itemData.name} className="h-3/4 shadow-lg w-full object-cover rounded-lg w-100"/>
                    <div className="flex justify-between flex-wrap">
                        {images.map(image=>{
                            return(
                                <img key={image.id} src={image.src} alt={itemData.name} className="mt-3 w-100 h-40 width-32 cursor-pointer rounded-lg object-cover" onClick={()=>handleImageDisplayed(image.src)}/>
                            )
                        })}
                    </div>
                </div>:<Loading1 className="flex flex-col items-center"/>}
                <div style={{background:theme.cartColor}} className="py-10 pl-10 pr-5 shadow-xl rounded-xl mt-5 lg:mt-0 relative">
                    <h1 style={{borderColor:theme.secondColor}} className="headers border-b-4 uppercase">{itemData.name}</h1>
                    <div className="mt-4 tracking-wider leading-7 font-mono">{itemData.description?<p>{itemData.description}</p>:<Loading1/>}</div>
                    <div className="mt-5 font-bold">
                        <p style={{borderColor:theme.secondColor}} className="border-b-4 inline font-semibold text-xl">Profit : {itemData.profit}$</p>
                        <div className={`flex sm:flex-row flex-col justify-between text-center ${loading?'mt-7':'mt-10'}`}>
                            <div className="flex items-center justify-center">
                                <select style={{background:theme.secondColor}} className="text-white w-full capitalize font-bold outline-none pl-2 pr-1 py-2" value={colorChosen} onChange={(e)=>setColorChosen(e.target.value)}>
                                    <option value="" disabled selected hidden>Select Cover Color</option>
                                    {itemData.colors?.map(color=>{
                                        return(
                                            <option key={color} className="capitalize font-bold border-0 hover:bg-yellow-400 outline-none">{color}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="sm:mt-0 mt-5">
                                <div style={{background:theme.secondColor}} className="counters sm:px-3 px-2" onClick={addCounter}>
                                    <FontAwesomeIcon icon={faPlus} />
                                </div>
                                <input style={{background:theme.secondColor}} className="text-center w-2/4 mx-1 py-1 sm:w-24 outline-none rounded-2xl text-white font-bold" type="number" placeholder="1" readOnly="readOnly" onChange={(e)=>setItemQuantity(e.target.value)} value={itemQuantity}/>
                                <div style={{background:theme.secondColor}} className="counters sm:px-3 px-2" onClick={subtractCounter}>
                                    <FontAwesomeIcon icon={faMinus} />
                                </div>
                            </div>
                        </div>
                        <div className="mt-5 flex sm:flex-row flex-col justify-between text-center">
                            <button style={{background:cartState && !itemState?theme.secondColor:'rgba(0,0,0,0.75)'}} className={`text-white default-button px-7 ${cartState && !itemState?'cursor-pointer':'cursor-not-allowed'}`} onClick={handleItemToCart}>{cartState && !itemState?'Add to Cart':'Added To Cart'}</button>
                            <Link to='/' style={{background:theme.secondColor}} className="text-white default-button px-7 sm:mt-0 mt-3">Back to Home</Link>  
                        </div>
                        <div className={`transition-all duration-300 text-white ${quantityError && cartState?'bg-red-600 mt-5 p-3 text-center h-auto shadow-md':'h-0 overflow-hidden'}`}>
                            <p>{quantityError}</p>
                        </div>
                        {loading?<Loading1/>:null}
                        <div className="mt-5">
                            <div className="absolute bottom-5 mt-5">
                                <p className="font-light text-sm">© Copyright 2021 <Link to="/" style={{color:theme.secondColor}}>Games Nation</Link>. All Rights Reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/*<div className="mt-20">
                <Header1 title="More Like This"/>
                <div className="mt-5">
                    <SliderContainer>
                        {games.filter(filteredGame=>filteredGame.category==itemData.category && filteredGame.name!==itemData.name).map((gameItem,gameIndexItem)=>{
                            return(
                                <SwiperSlide className="mb-7 order-10 outline-none" onClick={()=>handleRender(gameItem)} key={gameIndexItem}>
                                    <Card1 game={gameItem}/>
                                </SwiperSlide>
                            )
                        })}
                    </SliderContainer>
                </div>
            </div>*/}
        </div>
    )
}

export default Item