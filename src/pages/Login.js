import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'
import { useHistory } from 'react-router'
import { useEffect, useState } from "react"
import ThemeWrapper from '../components/Wrapper/ThemeWrapper'
import Form from '../components/Forms/Form1'
const Login = ()=>{
    const [email,setEmail]=useState('')
    const [password,setPassword]=useState('')
    const [emailError,setEmailError]=useState('')
    const [passwordError,setPasswordError]=useState('')
    const [loginError,setLoginError]=useState('')
    let history=useHistory()
    const handleSignin = (e) =>{
        e.preventDefault()
        e.preventDefault()
        if(email.trim()===''){
            setEmailError('Your email cannot be empty')
        }else{
            setEmailError('')
        }
        if(password.trim()==='' || password.length<6){
            setPasswordError('Your password cannot be empty or below 6 letters')
        }else{
            setPasswordError('')
        }
        if(email.trim()!=='' && password.trim()!=='' && !password.length<6){
            firebase.auth().signInWithEmailAndPassword(email,password).then(cred=>{
                setEmail('')
                setPassword('')
                history.push('/')
            })
        }
    }
    return(
        <ThemeWrapper>
            <Form 
            formType="Login"
            formLink="/signup"
            formLinkTitle="Signup"
            account="Doesn't have an account?"
            email={(e)=>setEmail(e.target.value)}
            emailValue={email}
            password={(e)=>setPassword(e.target.value)}
            passwordValue={password}
            formHandle={handleSignin}
            emailError={emailError}
            passwordError={passwordError}
            signError={loginError}/>
        </ThemeWrapper>
    )
}

    

export default Login