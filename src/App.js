import React, { lazy,Suspense  } from 'react'
import Loading2 from './components/Loadings/Loading2';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Theme from './components/contexts/Theme'
import ThemeWrapper from './components/Wrapper/ThemeWrapper'
import Footer1 from './components/Footers/Footer1';
import ScrollToTop from './components/ScrollTop/ScrollTop'
const Home = lazy(()=>import('./pages/Home'))
const Login = lazy(()=>import('./pages/Login'));
const Signup = lazy(()=>import('./pages/Signup'))
const Cart = lazy(()=>import('./pages/Cart'));
const Item = lazy(()=>import('./pages/Item'))
const Checkout = lazy(()=>import('./pages/Checkout'));
function App() {
  return (
    <div>
      <Router>
        <Theme>
          <Suspense fallback={Loading2}>
            <Loading2 bg='rgba(255,248,231,1)'>
              <Footer1/>
              <ThemeWrapper>
                <ScrollToTop>
                  <Switch>
                    <Route exact path="/">
                      <Home/>
                    </Route>
                    <Route exact path="/login">
                      <Login/>
                    </Route>
                    <Route exact path="/signup">
                      <Signup/>
                    </Route>
                    <Route exact path="/cart">
                      <Cart/>
                    </Route>
                    <Route exact path="/checkout">
                      <Checkout/>
                    </Route>
                    <Route exact path="/:game_name/:item_id">
                      <Item/>
                    </Route>
                  </Switch>
                </ScrollToTop>
              </ThemeWrapper>
            </Loading2>
          </Suspense>
        </Theme>
      </Router>
    </div>
  );
}

export default App;
