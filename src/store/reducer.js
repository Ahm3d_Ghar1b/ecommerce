import * as actionTypes from './actions'
const initialState = {
    games:[],
    cart:[]
}
const reducer = (state=initialState,actions) =>{
    switch(actions.type){
        case actionTypes.GETGAMES:
            return{
                games:actions.payload
            }
        case actionTypes.GETCARTITEMS:
            return{
                cart:actions.payload
            }
        default:
            return state
    }
}

export default reducer