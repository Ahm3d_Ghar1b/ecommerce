import axios from 'axios'
export const GETGAMES='GETGAMES' 
export const GETCARTITEMS='GETCARTITEMS'
const getGames = (games)=>{
    return{
        type:GETGAMES,
        payload:games
    }
}
const getCartItems = (cartItems)=>{
    return{
        type:GETCARTITEMS,
        payload:cartItems
    }
}
export const fetchGames = ()=>{
    return dispatch =>{
        axios.get("https://ecommerce-6f468-default-rtdb.firebaseio.com/games.json").then(response=>{
            const fetchedGames = []
            for (let k in response.data){
                fetchedGames.push({...response.data[k],id:k})
            }
            dispatch(getGames(fetchedGames))
        })
    }
}
export const fetchCart = ()=>{
    return dispatch =>{
        axios.get("https://ecommerce-6f468-default-rtdb.firebaseio.com/cart.json").then(response=>{
            const fetchedCartItems = []
            for (let k in response.data){
                fetchedCartItems.push({...response.data[k],id:k})
            }
            dispatch(getCartItems(fetchedCartItems))
        })
    }
}